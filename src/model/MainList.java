package model;

import java.util.ArrayList;

import javafx.beans.InvalidationListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public class MainList {
	public void testList() {
		//ChangeListener<Object> cl;
		final ObservableList<Player> playerList = FXCollections.observableArrayList();
//		final ObservableList<Player> playerList = FXCollections.observableList(
//			    new ArrayList<Player>(),
//			    (Player p) -> new Observable[]{p.nameProperty(), p.idProperty() 
//		});
		
		playerList.addListener((ListChangeListener<? super Player>) new ListChangeListener<Player>() {
			@Override
	        public void onChanged(Change<? extends Player> change) {
	            //System.out.println("Change" + change);
	            //
	            for(Player p: playerList) {
	            	if(!p.isNamePropListener()) {
	            		p.setNamePropListener(true);
	    		    	p.nameProperty().addListener(new ChangeListener<Object>(){
		    		        @Override public void changed(ObservableValue<?> o,Object oldVal, 
		    		                 Object newVal){
		    		        	
		    		            System.out.println("Name has changed! " + oldVal + " " + newVal);
		    		        }
	    		    	});	            
	            	}
	            	if(!p.isIdPropListener()) {
	            		p.setIdPropListener(true);
	    		    	p.idProperty().addListener(new ChangeListener<Object>(){
		    		        @Override public void changed(ObservableValue<?> o,Object oldVal, 
		    		                 Object newVal){
		    		        	
		    		            System.out.println("ID has changed! " + oldVal + " " + newVal);
		    		        }
	    		    	});	            
	            	}	            	
	            }
	            //
	        }
	    });
				
	    playerList.addListener((InvalidationListener) (observable) -> {
	        //System.out.println("New invalid state: " +  (observable).toString());
//	    	for(Player p: playerList) {
//            	if(!p.isNamePropListener()) {
//            		p.setNamePropListener(true);
//    		    	p.nameProperty().addListener(new ChangeListener<Object>(){
//	    		        @Override public void changed(ObservableValue<?> o,Object oldVal, 
//	    		                 Object newVal){
//	    		        	
//	    		            System.out.println("Name has changed! " + oldVal + " " + newVal);
//	    		        }
//    		    	});	            
//            	}
//            	if(!p.isIdPropListener()) {
//            		p.setIdPropListener(true);
//    		    	p.idProperty().addListener(new ChangeListener<Object>(){
//	    		        @Override public void changed(ObservableValue<?> o,Object oldVal, 
//	    		                 Object newVal){
//	    		        	
//	    		            System.out.println("ID has changed! " + oldVal + " " + newVal);
//	    		        }
//    		    	});	            
//            	}	            	
//            }
	    });
	    
	    playerList.add(new Player("Name1", 1));
		playerList.add(new Player("Name2", 2));
	    //Player p = new Player();
//	    for(Player p: playerList) {
//		    p.nameProperty().addListener(new ChangeListener<Object>(){
//		        @Override public void changed(ObservableValue<?> o,Object oldVal, 
//		                 Object newVal){
//		             System.out.println("Name has changed! " + oldVal + " " + newVal);
//		        }
//		    });
//	    }
	    
	    
	    playerList.get(1).setName("Name4");
	    playerList.get(0).setId(3);
	    playerList.get(0).setName("Name3");
	    playerList.get(1).setId(4);
	    //p.setName("nameChange");
	    for(Player p: playerList) {
	    	System.out.println(p.getName());
	    	System.out.println(p.getId());
	    }
	}
}
