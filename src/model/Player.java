package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Player extends SimpleObjectProperty<Player> {
	// Define a variable to store the property
	private StringProperty name = new SimpleStringProperty();
	private IntegerProperty id = new SimpleIntegerProperty();
	private boolean isNamePropListener = false;
	private boolean isIdPropListener = false;
	
	
	public Player() {
		
	}
	
	//add listeners inside constructor possibly??
	//also possible add/delete extra object to engage listener
	public Player(String name, int id) {
		this.name = new SimpleStringProperty(name);
		this.id = new SimpleIntegerProperty(id);
	}
	
	public boolean isIdPropListener() {
		return isIdPropListener;
	}

	public void setIdPropListener(boolean isIdPropListener) {
		this.isIdPropListener = isIdPropListener;
	}

	public boolean isNamePropListener() {
		return isNamePropListener;
	}
	
	public void setNamePropListener(boolean isNamePropListener) {
		this.isNamePropListener = isNamePropListener;
	}
	
	/**
     * @return the name
     */
    public final String getName() {
        return name.get();
    }
    /**
     * @param name 
     */
    public final void setName(String name) {
        this.name.set(name);
    }
    
    public StringProperty nameProperty() {
        return name;
    }
    
	public Integer getId() {
		return id.get();
	}
	public void setId(int id) {
		this.id.set(id);
	}
	public IntegerProperty idProperty() {
		return id;
	}
	
	

	
	
}
